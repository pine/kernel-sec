from debian import deb822
import os.path
import re


class issue(deb822.Deb822):
    nonReleaseFields = ['Candidate',
                        'References',
                        'Description',
                        'Notes',
                        'Bugs']
    reservedPrefixes = ['Ubuntu-']

    def __init__(self, path):
        self.file = open(path, 'r')
        self.d = deb822.Deb822(self.file)
        self.name = os.path.basename(path)

    def status(self, release):
        if release in self.d:
            return self.d[release]
        else:
            return ""

    def fieldIsRelease(self, field):
        if field in self.nonReleaseFields:
            return False
        for p in self.reservedPrefixes:
            if field[:len(p)] == p:
                return False
        return True

    def get_releases(self):
        releases = set()
        for field in self.d.keys():
            if self.fieldIsRelease(field):
                releases.add(field)
        return releases


_ignore_patterns = [re.compile('.*~$'),
                    re.compile('^#.*'),
                    re.compile('^00.*')]


def _ignore_issue_name(issue):
    for p in _ignore_patterns:
        if p.match(issue):
            return True
    return False


# Pad last part of CVE ID to 8 digits so string comparison keeps working
def _pad_cve_id(cve_id):
    return re.sub(r'-(\d+)$', lambda m: '-%08d' % int(m.group(1)), cve_id)


def get_issues(dir):
    issues = []
    L = [f for f in os.listdir(dir)
         if not os.path.isdir(os.path.join(dir, f))
         and not _ignore_issue_name(f)]
    L.sort(key=_pad_cve_id)

    return [ issue(os.path.join(dir, f)) for f in L ]


def parse_status(s):
    ws = '\s*'
    versions = '(?P<versions>\((\S*,\s*)*\S*\s*\))'
    changerefs = '(?P<changerefs>\[(\S*,\s*)*\S*\s*\])'
    state = '(?P<state>\S*)'

    statusre = re.compile(ws + state + ws +
                          '(' + versions + '?)' +
                          '(' + changerefs + '?)')
    m = statusre.match(s)
    if not m:
        raise SyntaxError
    else:
        return m.groupdict()
