#!/usr/bin/perl -w
# Analyse patch-tracker entries
# Problems reported to stderr
# Suggested entrie printed to stdout
# Must be run in directory with patch-tracker entries
#
# (C) 2006 Horms <horms@verge.net.au>
# Released under the terms of the GNU GPL v2

use strict;

my $BOILERPLATE = "00boilerplate";

sub readfile {
	my ($file) = (@_);
	my $l = [];
	my $h = {};
	my $key = undef;

	open BP, "<$file" or die "Could not open \"$file\" for reading\n";
	while (<BP>) {
		if (m/(^[a-zA-Z0-9.-]+:)(.*)/ and $1 ne "http:") {
			$key = $1;
			push @$l , $1;
			$h->{"$1"} = $2 . "\n";
		}
		elsif (defined $key) {
			$h->{"$key"} .= $_;
		}
		else {
			print STDERR "Leading crap: $_";
		}
	}
	close BP;

	return ($l, $h);
}

{
	my $bp_l;
	my $bp_h;
	my $l;
	my $h;

	($bp_l, $bp_h) = readfile($BOILERPLATE);

	foreach my $f (@ARGV) {
		($l, $h) = readfile($f);
		my $log_p = (scalar @ARGV > 1) ? "$f: " : "";
		for my $i (@$bp_l) {
			if (defined $h->{"$i"}) {
				print $i . $h->{"$i"};
				delete $h->{"$i"};
			}
			else {
				print STDERR $log_p . "Missing Field: \"$i\"\n";
				print $i . " XXX\n";
			}
		}
		for my $i (keys %$h) {
			print STDERR $log_p . "Extra Feild: \"$i\"\n";
			if (defined  $h->{"$i"}) {
				print $i . $h->{"$i"};
			}
			else {
				print $i . " XXX\n";
			}
		}
	}
}
