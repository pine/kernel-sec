Package        : linux
CVE ID:        : CVE-2018-15471 CVE-2018-18021

Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.

CVE-2018-15471 (XSA-270)

    Felix Wilhelm of Google Project Zero discovered a flaw in the hash
    handling of the xen-netback Linux kernel module. A malicious or
    buggy frontend may cause the (usually privileged) backend to make
    out of bounds memory accesses, potentially resulting in privilege
    escalation, denial of service, or information leaks.

    https://xenbits.xen.org/xsa/advisory-270.html

CVE-2018-18021

    It was discovered that the KVM subsystem on the arm64 platform does
    not properly handle the KVM_SET_ON_REG ioctl. An attacker who can
    create KVM based virtual machines can take advantage of this flaw
    for denial of service (hypervisor panic) or privilege escalation
    (arbitrarily redirect the hypervisor flow of control with full
    register control).

For the stable distribution (stretch), these problems have been fixed in
version 4.9.110-3+deb9u6.
