Package        : linux
CVE ID         : CVE-2017-7518 CVE-2017-7558 CVE-2017-10661 CVE-2017-11600
                 CVE-2017-12134 CVE-2017-12146 CVE-2017-12153 CVE-2017-12154
                 CVE-2017-14106 CVE-2017-14140 CVE-2017-14156 CVE-2017-14340
                 CVE-2017-14489 CVE-2017-14497 CVE-2017-1000111 CVE-2017-1000112
                 CVE-2017-1000251 CVE-2017-1000252 CVE-2017-1000370 CVE-2017-1000371
                 CVE-2017-1000380
Debian Bug     : 866511 875881

Several vulnerabilities have been discovered in the Linux kernel that
may lead to privilege escalation, denial of service or information
leaks.

CVE-2017-7518

    Andy Lutomirski discovered that KVM is prone to an incorrect debug
    exception (#DB) error occurring while emulating a syscall
    instruction. A process inside a guest can take advantage of this
    flaw for privilege escalation inside a guest.

CVE-2017-7558 (stretch only)

    Stefano Brivio of Red Hat discovered that the SCTP subsystem is
    prone to a data leak vulnerability due to an out-of-bounds read
    flaw, allowing to leak up to 100 uninitialized bytes to userspace.

CVE-2017-10661 (jessie only)

    Dmitry Vyukov of Google reported that the timerfd facility does
    not properly handle certain concurrent operations on a single file
    descriptor.  This allows a local attacker to cause a denial of
    service or potentially execute arbitrary code.

CVE-2017-11600

    Bo Zhang reported that the xfrm subsystem does not properly
    validate one of the parameters to a netlink message. Local users
    with the CAP_NET_ADMIN capability can use this to cause a denial
    of service or potentially to execute arbitrary code.

CVE-2017-12134 / #866511 / XSA-229

    Jan H. Schoenherr of Amazon discovered that when Linux is running
    in a Xen PV domain on an x86 system, it may incorrectly merge
    block I/O requests.  A buggy or malicious guest may trigger this
    bug in dom0 or a PV driver domain, causing a denial of service or
    potentially execution of arbitrary code.

    This issue can be mitigated by disabling merges on the underlying
    back-end block devices, e.g.:
        echo 2 > /sys/block/nvme0n1/queue/nomerges

CVE-2017-12146 (stretch only)

    Adrian Salido of Google reported a race condition in access to the
    "driver_override" attribute for platform devices in sysfs. If
    unprivileged users are permitted to access this attribute, this
    might allow them to gain privileges.

CVE-2017-12153

    bo Zhang reported that the cfg80211 (wifi) subsystem does not
    properly validate the parameters to a netlink message. Local users
    with the CAP_NET_ADMIN capability (in any user namespace with a
    wifi device) can use this to cause a denial of service.

CVE-2017-12154

    Jim Mattson of Google reported that the KVM implementation for
    Intel x86 processors did not correctly handle certain nested
    hypervisor configurations. A malicious guest (or nested guest in a
    suitable L1 hypervisor) could use this for denial of service.

CVE-2017-14106

    Andrey Konovalov discovered that a user-triggerable division by
    zero in the tcp_disconnect() function could result in local denial
    of service.

CVE-2017-14140

    Otto Ebeling reported that the move_pages() system call performed
    insufficient validation of the UIDs of the calling and target
    processes, resulting in a partial ASLR bypass. This made it easier
    for local users to exploit vulnerabilities in programs installed
    with the set-UID permission bit set.

CVE-2017-14156

    "sohu0106" reported an information leak in the atyfb video driver.
    A local user with access to a framebuffer device handled by this
    driver could use this to obtain sensitive information.

CVE-2017-14340

    Richard Wareing discovered that the XFS implementation allows the
    creation of files with the "realtime" flag on a filesystem with no
    realtime device, which can result in a crash (oops). A local user
    with access to an XFS filesystem that does not have a realtime
    device can use this for denial of service.

CVE-2017-14489

    ChunYu Wang of Red Hat discovered that the iSCSI subsystem does not
    properly validate the length of a netlink message, leading to
    memory corruption. A local user with permission to manage iSCSI
    devices can use this for denial of service or possibly to execute
    arbitrary code.

CVE-2017-14497 (stretch only)

    Benjamin Poirier of SUSE reported that vnet headers are not
    properly handled within the tpacket_rcv() function in the raw
    packet (af_packet) feature. A local user with the CAP_NET_RAW
    capability can take advantage of this flaw to cause a denial of
    service (buffer overflow, and disk and memory corruption) or have
    other impact.

CVE-2017-1000111

    Andrey Konovalov of Google reported a race condition in the raw
    packet (af_packet) feature. Local users with the CAP_NET_RAW
    capability can use this for denial of service or possibly to
    execute arbitrary code.

CVE-2017-1000112

    Andrey Konovalov of Google reported a race condition flaw in the
    UDP Fragmentation Offload (UFO) code. A local user can use this
    flaw for denial of service or possibly to execute arbitrary code.

CVE-2017-1000251 / #875881

    Armis Labs discovered that the Bluetooth subsystem does not
    properly validate L2CAP configuration responses, leading to a
    stack buffer overflow. This is one of several vulnerabilities
    dubbed "Blueborne". A nearby attacker can use this to cause a
    denial of service or possibly to execute arbitrary code on a
    system with Bluetooth enabled.

CVE-2017-1000252 (stretch only)

    Jan H. Schoenherr of Amazon reported that the KVM implementation
    for Intel x86 processors did not correctly validate interrupt
    injection requests. A local user with permission to use KVM could
    use this for denial of service.

CVE-2017-1000370

    The Qualys Research Labs reported that a large argument or
    environment list can result in ASLR bypass for 32-bit PIE binaries.

CVE-2017-1000371

    The Qualys Research Labs reported that a large argument
    orenvironment list can result in a stack/heap clash for 32-bit
    PIE binaries.

CVE-2017-1000380

    Alexander Potapenko of Google reported a race condition in the ALSA
    (sound) timer driver, leading to an information leak. A local user
    with permission to access sound devices could use this to obtain
    sensitive information.

Debian disables unprivileged user namespaces by default, but if they
are enabled (via the kernel.unprivileged_userns_clone sysctl) then
CVE-2017-11600, CVE-2017-14497 and CVE-2017-1000111 can be exploited
by any local user.

For the oldstable distribution (jessie), these problems have been fixed
in version 3.16.43-2+deb8u5.

For the stable distribution (stretch), these problems have been fixed in
version 4.9.30-2+deb9u5.
