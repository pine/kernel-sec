----------------------------------------------------------------------
Debian Security Advisory DSA-1844-1                security@debian.org
http://www.debian.org/security/                           Dann Frazier
July 28, 2009                       http://www.debian.org/security/faq
----------------------------------------------------------------------

Package        : linux-2.6.24
Vulnerability  : denial of service/privilege escalation
Problem type   : local/remote
Debian-specific: no
CVE Id(s)      : CVE-2009-1385 CVE-2009-1389 CVE-2009-1630 CVE-2009-1633
                 CVE-2009-1895 CVE-2009-1914 CVE-2009-1961 CVE-2009-2406
                 CVE-2009-2407

Several vulnerabilities have been discovered in the Linux kernel that
may lead to a denial of service or privilege escalation. The Common
Vulnerabilities and Exposures project identifies the following
problems:

CVE-2009-1385

    Neil Horman discovered a missing fix from the e1000 network driver.
    A remote user may cause a denial of service by way of a kernel panic
    triggered by specially crafted frame sizes.

CVE-2009-1389

    Michael Tokarev discovered an issue in the r8169 network driver.
    Remote users on the same LAN may cause a denial of service by way
    of a kernel panic triggered by receiving a large size frame.
    
CVE-2009-1630

    Frank Filz discovered that local users may be able to execute
    files without execute permission when accessed via an nfs4 mount.

CVE-2009-1633

    Jeff Layton and Suresh Jayaraman fixed several buffer overflows in
    the CIFS filesystem which allow remote servers to cause memory
    corruption.

CVE-2009-1895

    Julien Tinnes and Tavis Ormandy reported and issue in the Linux
    vulnerability code. Local users can take advantage of a setuid
    binary that can either be made to dereference a NULL pointer or
    drop privileges and return control to the user. This allows a
    user to bypass mmap_min_addr restrictions which can be exploited
    to execute arbitrary code.

CVE-2009-1914

    Mikulas Patocka discovered an issue in sparc64 kernels that allows
    local users to cause a denial of service (crash) by reading the
    /proc/iomem file.

CVE-2009-1961

    Miklos Szeredi reported an issue in the ocfs2 filesystem. Local
    users can create a denial of service (filesystem deadlock) using
    a particular sequence of splice system calls.
    
CVE-2009-2406
CVE-2009-2407

    Ramon de Carvalho Valle discovered two issues with the eCryptfs
    layered filesystem using the fsfuzzer utility. A local user with
    permissions to perform an eCryptfs mount may modify the contents
    of a eCryptfs file, overflowing the stack and potentially gaining
    elevated privileges.
    
For the stable distribution (etch), these problems have been fixed in
version 2.6.24-6~etchnhalf.8etch2.

We recommend that you upgrade your linux-2.6.24 packages.

Note: Debian 'etch' includes linux kernel packages based upon both the
2.6.18 and 2.6.24 linux releases.  All known security issues are
carefully tracked against both packages and both packages will receive
security updates until security support for Debian 'etch'
concludes. However, given the high frequency at which low-severity
security issues are discovered in the kernel and the resource
requirements of doing an update, lower severity 2.6.18 and 2.6.24
updates will typically release in a staggered or "leap-frog" fashion.

Upgrade instructions
--------------------

wget url
        will fetch the file for you
dpkg -i file.deb
        will install the referenced file.

If you are using the apt-get package manager, use the line for
sources.list as given below:

apt-get update
        will update the internal database
apt-get upgrade
        will install corrected packages

You may use an automated update by adding the resources from the
footer to the proper configuration.

Debian GNU/Linux 4.0 alias etch
-------------------------------

Oldstable updates are available for alpha, amd64, arm, hppa, i386, ia64, mips, mipsel, powerpc, s390 and sparc.

Source archives:

  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-2.6.24_2.6.24-6~etchnhalf.8etch2.diff.gz
    Size/MD5 checksum:  4046697 0c540aa51d64fd0f41fefda0370a7d57
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-2.6.24_2.6.24-6~etchnhalf.8etch2.dsc
    Size/MD5 checksum:     5117 8149bb152305e615760fd5accc516b17
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-2.6.24_2.6.24.orig.tar.gz
    Size/MD5 checksum: 59630522 6b8751d1eb8e71498ba74bbd346343af

Architecture independent packages:

  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-patch-debian-2.6.24_2.6.24-6~etchnhalf.8etch2_all.deb
    Size/MD5 checksum:   931690 8230f79880ab579b104e9b34029cc97d
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-manual-2.6.24_2.6.24-6~etchnhalf.8etch2_all.deb
    Size/MD5 checksum:  1572166 59bdbfc8850a9eb4c7f09229f8481a04
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-source-2.6.24_2.6.24-6~etchnhalf.8etch2_all.deb
    Size/MD5 checksum: 46892520 c736086affec0e829b80074008edc96d
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-support-2.6.24-etchnhalf.1_2.6.24-6~etchnhalf.8etch2_all.deb
    Size/MD5 checksum:    97696 ac7c8e4af4d15f8e77817f3a2060621e
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-doc-2.6.24_2.6.24-6~etchnhalf.8etch2_all.deb
    Size/MD5 checksum:  4469630 76391a2afe93b14ef942260a2ab0f6c4
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-tree-2.6.24_2.6.24-6~etchnhalf.8etch2_all.deb
    Size/MD5 checksum:    82766 fccf13a8baefeb96443c00718b47178a

alpha architecture (DEC Alpha)

  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-alpha-legacy_2.6.24-6~etchnhalf.8etch2_alpha.deb
    Size/MD5 checksum: 26737560 705e6c1f456e523e4cd6e2199247ad8a
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-alpha-smp_2.6.24-6~etchnhalf.8etch2_alpha.deb
    Size/MD5 checksum: 27342178 b22b7be9f15a3fcd569e83b0981c7b2b
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-common_2.6.24-6~etchnhalf.8etch2_alpha.deb
    Size/MD5 checksum:  3454340 5d9c4195f523265e691a73d165a32bd5
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-alpha-generic_2.6.24-6~etchnhalf.8etch2_alpha.deb
    Size/MD5 checksum:   331144 9b199341a9906f69b92956bee989678f
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-alpha-smp_2.6.24-6~etchnhalf.8etch2_alpha.deb
    Size/MD5 checksum:   330548 2e97270481c6406f4703f7ffc642a306
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all-alpha_2.6.24-6~etchnhalf.8etch2_alpha.deb
    Size/MD5 checksum:    82254 6ef54422427a87fb7013208abbb90bfe
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-alpha-generic_2.6.24-6~etchnhalf.8etch2_alpha.deb
    Size/MD5 checksum: 26758348 3fa44a2c192d5296abff11160c4143f7
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all_2.6.24-6~etchnhalf.8etch2_alpha.deb
    Size/MD5 checksum:    82234 2d73cbb156467a0e189b66d64d725957
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-alpha-legacy_2.6.24-6~etchnhalf.8etch2_alpha.deb
    Size/MD5 checksum:   332176 8abd6e28989d5ffa359dbdf523b8802a

amd64 architecture (AMD x86_64 (AMD64))

  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all_2.6.24-6~etchnhalf.8etch2_amd64.deb
    Size/MD5 checksum:    82232 f31859776e8c538793ce7bc12e4d16e6
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all-amd64_2.6.24-6~etchnhalf.8etch2_amd64.deb
    Size/MD5 checksum:    82238 0dd51e0668293dc4a112ba7c177a2d62
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-amd64_2.6.24-6~etchnhalf.8etch2_amd64.deb
    Size/MD5 checksum:   355216 e912a184ffa55eefcbf4d075575f956b
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-common_2.6.24-6~etchnhalf.8etch2_amd64.deb
    Size/MD5 checksum:  3649934 25d6d6f81163cd422b987cffe8555482
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-amd64_2.6.24-6~etchnhalf.8etch2_amd64.deb
    Size/MD5 checksum: 19596152 128c3dcd4b54fbca2a8cccf553b15c15

arm architecture (ARM)

  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-ixp4xx_2.6.24-6~etchnhalf.8etch2_arm.deb
    Size/MD5 checksum:   308764 081a21f64a1939858fd628b2b17b1e9d
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all_2.6.24-6~etchnhalf.8etch2_arm.deb
    Size/MD5 checksum:    82362 a02c06864fb8adb347c7b3b83707ec71
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-common_2.6.24-6~etchnhalf.8etch2_arm.deb
    Size/MD5 checksum:  3937452 4296fde893b88cba41a5164cd6c68266
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-footbridge_2.6.24-6~etchnhalf.8etch2_arm.deb
    Size/MD5 checksum:   297832 882e93119837e060496995474bfbde0c
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all-arm_2.6.24-6~etchnhalf.8etch2_arm.deb
    Size/MD5 checksum:    82388 5682b9cb3c9efdf951541c5e951858d5
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-footbridge_2.6.24-6~etchnhalf.8etch2_arm.deb
    Size/MD5 checksum:  9356202 12691f5684650f5f808b4ccc3d77e6f6
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-iop32x_2.6.24-6~etchnhalf.8etch2_arm.deb
    Size/MD5 checksum: 10777668 be1c3aa597e81f449a1712c059b6d219
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-ixp4xx_2.6.24-6~etchnhalf.8etch2_arm.deb
    Size/MD5 checksum: 10786276 fdfef0d9f0a0f740cdf096efe4076849
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-iop32x_2.6.24-6~etchnhalf.8etch2_arm.deb
    Size/MD5 checksum:   306278 243cc2aef642ad3dc3e6faa9f5b7d2ac

hppa architecture (HP PA RISC)

  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-parisc-smp_2.6.24-6~etchnhalf.8etch2_hppa.deb
    Size/MD5 checksum:   259166 403c84cc8eec53736e11babcd7133c36
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all-hppa_2.6.24-6~etchnhalf.8etch2_hppa.deb
    Size/MD5 checksum:    82390 c1e6ec2b43d6dc4849ab58fcca7708fd
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-parisc_2.6.24-6~etchnhalf.8etch2_hppa.deb
    Size/MD5 checksum:   257800 dcf392b46aaabf4187d0b739d7b3f0b6
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all_2.6.24-6~etchnhalf.8etch2_hppa.deb
    Size/MD5 checksum:    82356 e67f07544c8505aebf46d0fc5cc6ff9b
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-parisc64_2.6.24-6~etchnhalf.8etch2_hppa.deb
    Size/MD5 checksum:   258558 a2e28e8a24b42aa4f092218fdb7b24b0
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-parisc64_2.6.24-6~etchnhalf.8etch2_hppa.deb
    Size/MD5 checksum: 14371812 91a757a164d87fcca9b7c8dcf9737e52
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-parisc64-smp_2.6.24-6~etchnhalf.8etch2_hppa.deb
    Size/MD5 checksum:   261468 72a0d875029ef878bcfa7734ad0e3221
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-common_2.6.24-6~etchnhalf.8etch2_hppa.deb
    Size/MD5 checksum:  3444724 e4cbc7652241146662a324980caf28e8
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-parisc64-smp_2.6.24-6~etchnhalf.8etch2_hppa.deb
    Size/MD5 checksum: 14830286 ee2c1dd21c02e82c845897b92fc44d18
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-parisc-smp_2.6.24-6~etchnhalf.8etch2_hppa.deb
    Size/MD5 checksum: 13846760 3133b7187049be153f3d946556c58d5a
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-parisc_2.6.24-6~etchnhalf.8etch2_hppa.deb
    Size/MD5 checksum: 13335008 a71482e5f95d01ae60ef6f9e84851905

i386 architecture (Intel ia32)

  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-686-bigmem_2.6.24-6~etchnhalf.8etch2_i386.deb
    Size/MD5 checksum: 19345260 c8d133c7df467610a2b58b02d437f140
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-486_2.6.24-6~etchnhalf.8etch2_i386.deb
    Size/MD5 checksum: 19349370 99465914e158a8a8095d704c7a4eca58
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all_2.6.24-6~etchnhalf.8etch2_i386.deb
    Size/MD5 checksum:    82350 eb75675cf418a8fe18a45cf78e113b14
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-amd64_2.6.24-6~etchnhalf.8etch2_i386.deb
    Size/MD5 checksum: 19589510 ed83c2d0ba92a0f4b5fb5daed6b86d5c
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-amd64_2.6.24-6~etchnhalf.8etch2_i386.deb
    Size/MD5 checksum:   347746 23befd72e069faf404ede7a276b78311
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-common_2.6.24-6~etchnhalf.8etch2_i386.deb
    Size/MD5 checksum:  3647494 9cf283a4c89c8281db35e5fb2ace4335
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all-i386_2.6.24-6~etchnhalf.8etch2_i386.deb
    Size/MD5 checksum:    82384 199633269844e3e36cf936f5a63857a7
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-686_2.6.24-6~etchnhalf.8etch2_i386.deb
    Size/MD5 checksum:   361018 4f7299551139e79ad329057ff07de93d
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-686-bigmem_2.6.24-6~etchnhalf.8etch2_i386.deb
    Size/MD5 checksum:   359956 5272e0df584721713692ccb80e8fdc45
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-486_2.6.24-6~etchnhalf.8etch2_i386.deb
    Size/MD5 checksum:   361502 4c9a0586154b5fce6d50610d6728bf75
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-686_2.6.24-6~etchnhalf.8etch2_i386.deb
    Size/MD5 checksum: 19278068 41f6b91dceeaba5592a15bdbb4170c3a

ia64 architecture (Intel ia64)

  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all_2.6.24-6~etchnhalf.8etch2_ia64.deb
    Size/MD5 checksum:    82360 e96d164aa97cbda5c4e0d4a4d8d47298
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-common_2.6.24-6~etchnhalf.8etch2_ia64.deb
    Size/MD5 checksum:  3570154 a7bdb15fd5dc46ea33b570390700255a
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-mckinley_2.6.24-6~etchnhalf.8etch2_ia64.deb
    Size/MD5 checksum: 32289122 8a66a1efccab804aece0f9c9f9a0c2c3
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all-ia64_2.6.24-6~etchnhalf.8etch2_ia64.deb
    Size/MD5 checksum:    82378 11c070123743f3060aee864a6fa7621e
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-mckinley_2.6.24-6~etchnhalf.8etch2_ia64.deb
    Size/MD5 checksum:   322394 7e68b691385f67de57eca947718b3cf3
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-itanium_2.6.24-6~etchnhalf.8etch2_ia64.deb
    Size/MD5 checksum: 32112122 81952edc15d24552bc7e0e250d4e4c2f
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-itanium_2.6.24-6~etchnhalf.8etch2_ia64.deb
    Size/MD5 checksum:   322756 0332b3131abca0dde23a3980e3269d6a

mips architecture (MIPS (Big Endian))

  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-4kc-malta_2.6.24-6~etchnhalf.8etch2_mips.deb
    Size/MD5 checksum: 22151540 85f675d6aa5e858c776b25ed7b55fd01
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-sb1a-bcm91480b_2.6.24-6~etchnhalf.8etch2_mips.deb
    Size/MD5 checksum: 17152584 968f51589e2246eb98d54f8f7a0deac3
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-sb1-bcm91250a_2.6.24-6~etchnhalf.8etch2_mips.deb
    Size/MD5 checksum:   246524 7d37dee7fa5f20d5484a054697944a4a
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-r5k-ip32_2.6.24-6~etchnhalf.8etch2_mips.deb
    Size/MD5 checksum: 11958562 189d907c882ca1f971e5df5b020db861
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-sb1a-bcm91480b_2.6.24-6~etchnhalf.8etch2_mips.deb
    Size/MD5 checksum:   246320 9b22546464feae45ee05c1318ce4eeb1
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-r4k-ip22_2.6.24-6~etchnhalf.8etch2_mips.deb
    Size/MD5 checksum:   214790 9f3182d818ff7b6a05993763a6120725
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-sb1-bcm91250a_2.6.24-6~etchnhalf.8etch2_mips.deb
    Size/MD5 checksum: 17168120 bc71f4391f0b74712a77ce0a98104c42
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all_2.6.24-6~etchnhalf.8etch2_mips.deb
    Size/MD5 checksum:    82240 56ffbcabce3c1df699ab5005b86b5fcb
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-r4k-ip22_2.6.24-6~etchnhalf.8etch2_mips.deb
    Size/MD5 checksum: 10524568 1dcc5f02fea750b73b3d26bbf9436744
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-4kc-malta_2.6.24-6~etchnhalf.8etch2_mips.deb
    Size/MD5 checksum:   309414 2f3e51dd53a0aee437acd14822bdc812
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all-mips_2.6.24-6~etchnhalf.8etch2_mips.deb
    Size/MD5 checksum:    82294 9d3e3e23c0fc8a0f1e1841e28fd3a2e2
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-common_2.6.24-6~etchnhalf.8etch2_mips.deb
    Size/MD5 checksum:  3803692 496de2fac6b2631145112d3df8c30a74
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-5kc-malta_2.6.24-6~etchnhalf.8etch2_mips.deb
    Size/MD5 checksum:   309366 ad8430704237cac6706abefb3cb3a66a
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-r5k-ip32_2.6.24-6~etchnhalf.8etch2_mips.deb
    Size/MD5 checksum:   225310 6f208c9d5eada9b92d3e98530a7421ab
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-5kc-malta_2.6.24-6~etchnhalf.8etch2_mips.deb
    Size/MD5 checksum: 27775866 480af1976cfb2ad886bbeab57828e0a9

mipsel architecture (MIPS (Little Endian))

  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-5kc-malta_2.6.24-6~etchnhalf.8etch2_mipsel.deb
    Size/MD5 checksum: 26985726 697670a3bb624d4d3d78f46f3dd1ffa0
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-sb1a-bcm91480b_2.6.24-6~etchnhalf.8etch2_mipsel.deb
    Size/MD5 checksum: 16630534 64876417bfc90be839f742b7f3df4017
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all-mipsel_2.6.24-6~etchnhalf.8etch2_mipsel.deb
    Size/MD5 checksum:    82296 3b389589e0d350fd94e1e6a55064b597
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-r5k-cobalt_2.6.24-6~etchnhalf.8etch2_mipsel.deb
    Size/MD5 checksum:   246006 6b907d81792642929e73f8f7138a0686
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all_2.6.24-6~etchnhalf.8etch2_mipsel.deb
    Size/MD5 checksum:    82244 06ea877ec50d7981c308674e878de0fd
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-4kc-malta_2.6.24-6~etchnhalf.8etch2_mipsel.deb
    Size/MD5 checksum: 21734936 1f58a04e3f3e233fc379f7585b37f67c
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-4kc-malta_2.6.24-6~etchnhalf.8etch2_mipsel.deb
    Size/MD5 checksum:   309876 33999e485bc68c26f1355c34591fa9d1
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-sb1a-bcm91480b_2.6.24-6~etchnhalf.8etch2_mipsel.deb
    Size/MD5 checksum:   246316 f5ee5c81b6f69590e7483c63914aa4f1
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-r5k-cobalt_2.6.24-6~etchnhalf.8etch2_mipsel.deb
    Size/MD5 checksum: 13317230 d5c0090dd1f3487c1a1b98e663d302a9
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-common_2.6.24-6~etchnhalf.8etch2_mipsel.deb
    Size/MD5 checksum:  3803662 c04871193e6cd5fe88e7d757fe94dc15
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-sb1-bcm91250a_2.6.24-6~etchnhalf.8etch2_mipsel.deb
    Size/MD5 checksum:   246138 83d394f1c4638687b1bd71213d6fc9fd
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-sb1-bcm91250a_2.6.24-6~etchnhalf.8etch2_mipsel.deb
    Size/MD5 checksum: 16566994 4ad893a3f64121974125e7df7a246c32
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-5kc-malta_2.6.24-6~etchnhalf.8etch2_mipsel.deb
    Size/MD5 checksum:   308542 d1aa85d73a85243f9e11e6d319829560

powerpc architecture (PowerPC)

  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-powerpc-miboot_2.6.24-6~etchnhalf.8etch2_powerpc.deb
    Size/MD5 checksum: 17459600 932f0c6e71fa2a92bcbf91245d4a6f34
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-powerpc_2.6.24-6~etchnhalf.8etch2_powerpc.deb
    Size/MD5 checksum: 19195098 3894f438fae8ccd897ae1b193e05a06c
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-powerpc-smp_2.6.24-6~etchnhalf.8etch2_powerpc.deb
    Size/MD5 checksum: 19486104 e186a5e57fc6ef416a9f0611b4e32b00
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-powerpc64_2.6.24-6~etchnhalf.8etch2_powerpc.deb
    Size/MD5 checksum:   321892 920d908f2b7f39a4ea245a452707de9c
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-powerpc-smp_2.6.24-6~etchnhalf.8etch2_powerpc.deb
    Size/MD5 checksum:   321508 053a1e68eb59bc3616762c77c478b77c
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-common_2.6.24-6~etchnhalf.8etch2_powerpc.deb
    Size/MD5 checksum:  3672616 9480b5733676cd5d73e984cd6b36cdb2
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-powerpc64_2.6.24-6~etchnhalf.8etch2_powerpc.deb
    Size/MD5 checksum: 21169994 225176293c099a0a66a6e427f8f342c9
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-powerpc_2.6.24-6~etchnhalf.8etch2_powerpc.deb
    Size/MD5 checksum:   321100 ed5daac657617ab8ea09dedbbd5825e9
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all_2.6.24-6~etchnhalf.8etch2_powerpc.deb
    Size/MD5 checksum:    82240 d5758f3021b63ba5a46f4bb59a0264bd
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all-powerpc_2.6.24-6~etchnhalf.8etch2_powerpc.deb
    Size/MD5 checksum:    82278 68134af3769b6654c610701382a41932
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-powerpc-miboot_2.6.24-6~etchnhalf.8etch2_powerpc.deb
    Size/MD5 checksum:   294694 6e01dfd311f1094cb5cf39e6ab13030f

s390 architecture (IBM S/390)

  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-s390-tape_2.6.24-6~etchnhalf.8etch2_s390.deb
    Size/MD5 checksum:  1502142 d742c3ccecaeae89dd72efa337ad8d77
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-s390x_2.6.24-6~etchnhalf.8etch2_s390.deb
    Size/MD5 checksum:   194364 95a4fc8d1329f3926c95d0d866ea95c1
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-common_2.6.24-6~etchnhalf.8etch2_s390.deb
    Size/MD5 checksum:  3429940 46b5705e72023ec8eabbeaaae179df0f
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-s390_2.6.24-6~etchnhalf.8etch2_s390.deb
    Size/MD5 checksum:   194060 0c71ba7ea8c4108e88ce439c83e36101
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all_2.6.24-6~etchnhalf.8etch2_s390.deb
    Size/MD5 checksum:    82228 f6a160399eb50aed5c7ce0a802977eec
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all-s390_2.6.24-6~etchnhalf.8etch2_s390.deb
    Size/MD5 checksum:    82242 20fa5214b693fd929327b65dff5ec749
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-s390x_2.6.24-6~etchnhalf.8etch2_s390.deb
    Size/MD5 checksum:  7200766 c6697bcd7109bad0fd2742368bfc7173
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-s390_2.6.24-6~etchnhalf.8etch2_s390.deb
    Size/MD5 checksum:  6950228 2c118466985f0d41c074e3d7ae019d4e

sparc architecture (Sun SPARC/UltraSPARC)

  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all_2.6.24-6~etchnhalf.8etch2_sparc.deb
    Size/MD5 checksum:    82354 917940e7cc2b2ae64b7b13119c8d5cdf
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-sparc64-smp_2.6.24-6~etchnhalf.8etch2_sparc.deb
    Size/MD5 checksum:   263544 e8b14218397904ab5f792a659c713900
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-sparc64-smp_2.6.24-6~etchnhalf.8etch2_sparc.deb
    Size/MD5 checksum: 13317316 74c832fd75da1e42442c1fc2fb985454
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-common_2.6.24-6~etchnhalf.8etch2_sparc.deb
    Size/MD5 checksum:  3650988 92f721bff0660a92cff31845e3db2b09
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-sparc64_2.6.24-6~etchnhalf.8etch2_sparc.deb
    Size/MD5 checksum:   261690 519b6d40d8fc3af21f475b10bfaef609
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-headers-2.6.24-etchnhalf.1-all-sparc_2.6.24-6~etchnhalf.8etch2_sparc.deb
    Size/MD5 checksum:    82374 8ff85603936f91294ec959d8fbca1db5
  http://security.debian.org/pool/updates/main/l/linux-2.6.24/linux-image-2.6.24-etchnhalf.1-sparc64_2.6.24-6~etchnhalf.8etch2_sparc.deb
    Size/MD5 checksum: 13019920 db00ad3ea888c38b94f6e9c2aebbd834


  These changes will probably be included in the oldstable distribution on
  its next update.

---------------------------------------------------------------------------------
For apt-get: deb http://security.debian.org/ stable/updates main
For dpkg-ftp: ftp://security.debian.org/debian-security dists/stable/updates/main
Mailing list: debian-security-announce@lists.debian.org
Package info: `apt-cache show <pkg>' and http://packages.debian.org/<pkg>
