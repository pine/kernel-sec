----------------------------------------------------------------------
Debian Security Advisory DSA-1494-2                security@debian.org
http://www.debian.org/security/           Florian Weimer, dann frazier
February 12, 2008                   http://www.debian.org/security/faq
----------------------------------------------------------------------

Package        : linux-2.6
Vulnerability  : missing access checks
Problem type   : local
Debian-specific: no
CVE Id(s)      : CVE-2008-0010 CVE-2008-0163 CVE-2008-0600

The vmsplice system call did not properly verify address arguments
passed by user space processes, which allowed local attackers to
overwrite arbitrary kernel memory, gaining root privileges
(CVE-2008-0010, CVE-2008-0600).

In the vserver-enabled kernels, a missing access check on certain
symlinks in /proc enabled local attackers to access resources in other
vservers (CVE-2008-0163).

For the stable distribution (etch), this problem has been fixed in version
2.6.18.dfsg.1-18etch1.

In addition to these fixes, this update also incorporates changes from the
upcoming point release of the stable distribution.

Some architecture builds were not yet available at the time of DSA-1494-1.
This update to DSA-1494 provides linux-2.6 packages for these remaining
architectures, as well as additional binary packages that are built
from source code provided by linux-2.6.

The old stable distribution (sarge) is not affected by this problem.

The unstable (sid) and testing distributions will be fixed soon.

We recommend that you upgrade your linux-2.6, fai-kernels, and
user-mode-linux packages.

Upgrade instructions
--------------------

wget url
        will fetch the file for you
dpkg -i file.deb
        will install the referenced file.

If you are using the apt-get package manager, use the line for
sources.list as given below:

apt-get update
        will update the internal database
apt-get upgrade
        will install corrected packages

The following matrix lists additional source packages that were rebuilt for
compatability with or to take advantage of this update:

                                             Debian 4.0 (etch)
     fai-kernels                             1.17+etch.18etch1
     user-mode-linux                         2.6.18-1um-2etch.18etch1

You may use an automated update by adding the resources from the
footer to the proper configuration.

Debian GNU/Linux 4.0 alias etch
-------------------------------

Stable updates are available for alpha, amd64, arm, hppa, i386, ia64,
mips, mipsel, powerpc, sparc and s390.

Source archives:

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-2.6_2.6.18.dfsg.1-18etch1.diff.gz
    Size/MD5 checksum:  5379550 6a28d0278e4abe270c0c1f69ed463b9c
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-2.6_2.6.18.dfsg.1-18etch1.dsc
    Size/MD5 checksum:     5680 684a9ddb3b6975ce30764b26377f9162
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-2.6_2.6.18.dfsg.1.orig.tar.gz
    Size/MD5 checksum: 52225460 6a1ab0948d6b5b453ea0fce0fcc29060
  http://security.debian.org/pool/updates/main/f/fai-kernels/fai-kernels_1.17+etch.18etch1.dsc
    Size/MD5 checksum:      740 42ad7f3b4925c86466a12f6af1f60d34
  http://security.debian.org/pool/updates/main/f/fai-kernels/fai-kernels_1.17+etch.18etch1.tar.gz
    Size/MD5 checksum:    56178 1d940e99b60ea13d97af2a2c7091b7ca
  http://security.debian.org/pool/updates/main/u/user-mode-linux/user-mode-linux_2.6.18-1um-2etch.18etch1.dsc
    Size/MD5 checksum:      892 a316e3449f9cd0bbf497ad704c1d78ec
  http://security.debian.org/pool/updates/main/u/user-mode-linux/user-mode-linux_2.6.18-1um-2etch.18etch1.diff.gz
    Size/MD5 checksum:    16048 b62c78f80dbe59c81827b4d7cf1c3997
  http://security.debian.org/pool/updates/main/u/user-mode-linux/user-mode-linux_2.6.18-1um.orig.tar.gz
    Size/MD5 checksum:    14435 4d10c30313e11a24621f7218c31f3582

Architecture independent packages:

  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-doc-2.6.18_2.6.18.dfsg.1-18etch1_all.deb
    Size/MD5 checksum:  3753320 1e23b46c2d099b80cc0502c1ebb72e1b
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-tree-2.6.18_2.6.18.dfsg.1-18etch1_all.deb
    Size/MD5 checksum:    53924 5bd9cd783c6e8fdc37ccfe767578616d
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-patch-debian-2.6.18_2.6.18.dfsg.1-18etch1_all.deb
    Size/MD5 checksum:  1582740 8f6c460f657f081dcb367688ddf695a7
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-source-2.6.18_2.6.18.dfsg.1-18etch1_all.deb
    Size/MD5 checksum: 42181646 6d8046bfab1037093850d4194ab7e205
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-manual-2.6.18_2.6.18.dfsg.1-18etch1_all.deb
    Size/MD5 checksum:  1105710 744ef385a2799906634ea3bb0c96e481
  http://security.debian.org/pool/updates/main/l/linux-2.6/linux-support-2.6.18-6_2.6.18.dfsg.1-18etch1_all.deb
    Size/MD5 checksum:  3736216 ca1179eeb523abe2ec79d32c6291b21f

alpha architecture (DEC Alpha)

    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6_2.6.18.dfsg.1-18etch1_alpha.deb
      Size/MD5 checksum:  3027008 58817d16f3a96938ce15165feab3df05
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all_2.6.18.dfsg.1-18etch1_alpha.deb
      Size/MD5 checksum:    53432 6a4fa9d91e94001156df2fc0b2734881
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all-alpha_2.6.18.dfsg.1-18etch1_alpha.deb
      Size/MD5 checksum:    53458 dd653b1cd4270d660fa397604ddee9d2
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-alpha-generic_2.6.18.dfsg.1-18etch1_alpha.deb
      Size/MD5 checksum:   266170 221de745eb1b57fd2994822cf41b1aa4
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-alpha-legacy_2.6.18.dfsg.1-18etch1_alpha.deb
      Size/MD5 checksum:   266486 0f72f068044bfa6d94affaf9329f6208
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-alpha-smp_2.6.18.dfsg.1-18etch1_alpha.deb
      Size/MD5 checksum:   265630 aee9d2a87dadb3ca5b3ca5f05b0a1427
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-vserver_2.6.18.dfsg.1-18etch1_alpha.deb
      Size/MD5 checksum:  3050962 7f0cba3e1da1d554877fe71748675ff3
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-vserver-alpha_2.6.18.dfsg.1-18etch1_alpha.deb
      Size/MD5 checksum:   266840 456d1471ee10e314276f45b26174b10d
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-alpha-generic_2.6.18.dfsg.1-18etch1_alpha.deb
      Size/MD5 checksum: 23490812 cb4bc1a4c532173af1731fca79e721db
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-alpha-legacy_2.6.18.dfsg.1-18etch1_alpha.deb
      Size/MD5 checksum: 23469638 c16005b855758657c515df4968476699
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-alpha-smp_2.6.18.dfsg.1-18etch1_alpha.deb
      Size/MD5 checksum: 23843418 a3deed56c7f65efc99fca3c80a0caa88
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-vserver-alpha_2.6.18.dfsg.1-18etch1_alpha.deb
      Size/MD5 checksum: 23535814 9d284588da0acfb391f15c307b7329be

  AMD64 architecture:

    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6_2.6.18.dfsg.1-18etch1_amd64.deb
      Size/MD5 checksum:  3224978 8d7e1cfff6006f38972cb73fe92501c7
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all_2.6.18.dfsg.1-18etch1_amd64.deb
      Size/MD5 checksum:    53426 56d2366d6417b28cb9e8160a81cc530e
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all-amd64_2.6.18.dfsg.1-18etch1_amd64.deb
      Size/MD5 checksum:    53452 e1e93e54c0a89dc846a23f22d3107758
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-amd64_2.6.18.dfsg.1-18etch1_amd64.deb
      Size/MD5 checksum:   274128 9069b9a2f7d63ec555ded35041574afc
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-vserver_2.6.18.dfsg.1-18etch1_amd64.deb
      Size/MD5 checksum:  3249644 8fc467cf40d99c4d0d9c7cee5b350ed8
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-vserver-amd64_2.6.18.dfsg.1-18etch1_amd64.deb
      Size/MD5 checksum:   274384 22a5f1bbcdea0543ccab87bf35e2b0ae
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-xen_2.6.18.dfsg.1-18etch1_amd64.deb
      Size/MD5 checksum:  3394088 3a84135fbc74c45d4b725add41f3c7b5
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-xen-amd64_2.6.18.dfsg.1-18etch1_amd64.deb
      Size/MD5 checksum:   274824 d75a7ae583f4dc2f689a361c66995274
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-xen-vserver_2.6.18.dfsg.1-18etch1_amd64.deb
      Size/MD5 checksum:  3418196 57a52cd8359988c8536c542e36335bef
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-xen-vserver-amd64_2.6.18.dfsg.1-18etch1_amd64.deb
      Size/MD5 checksum:   274928 4ef9ee65a61c3069fa5e1ec26daf0fc4
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-amd64_2.6.18.dfsg.1-18etch1_amd64.deb
      Size/MD5 checksum: 16909236 af6e7ff6e6cbb016bf6ff224c8819706
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-vserver-amd64_2.6.18.dfsg.1-18etch1_amd64.deb
      Size/MD5 checksum: 16946162 cef425d442bcd009feadbd75b5f67b01
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-xen-amd64_2.6.18.dfsg.1-18etch1_amd64.deb
      Size/MD5 checksum:  1650400 31a44d5fe186084528c494aeb6bf4e57
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-xen-vserver-amd64_2.6.18.dfsg.1-18etch1_amd64.deb
      Size/MD5 checksum:  1682468 d5661572d7f608177e6a808d6eea7fd3
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-modules-2.6.18-6-xen-amd64_2.6.18.dfsg.1-18etch1_amd64.deb
      Size/MD5 checksum: 15348084 7f6ef2c74aad2a8e7bdaff7f10ec1ce2
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-modules-2.6.18-6-xen-vserver-amd64_2.6.18.dfsg.1-18etch1_amd64.deb
      Size/MD5 checksum: 15364328 a459d8f359ccb91592e04efc63a218c8
    http://security.debian.org/pool/updates/main/l/linux-2.6/xen-linux-system-2.6.18-6-xen-amd64_2.6.18.dfsg.1-18etch1_amd64.deb
      Size/MD5 checksum:    53410 7984a6fbe17ddded4c4b195eeec275da
    http://security.debian.org/pool/updates/main/l/linux-2.6/xen-linux-system-2.6.18-6-xen-vserver-amd64_2.6.18.dfsg.1-18etch1_amd64.deb
      Size/MD5 checksum:    53424 572285472f4fd6d85daa5b942f592f57
    http://security.debian.org/pool/updates/main/f/fai-kernels/fai-kernels_1.17+etch.18etch1_amd64.deb
      Size/MD5 checksum:  5959872 ef2a13197b89ccb3e66a9a142717364c

  ARM architecture:

    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6_2.6.18.dfsg.1-18etch1_arm.deb
      Size/MD5 checksum:  3409656 ce344f82d018ccc34a92e5a780245b2e
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all_2.6.18.dfsg.1-18etch1_arm.deb
      Size/MD5 checksum:    53434 ec61a5e5165024f292b137635926f08a
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all-arm_2.6.18.dfsg.1-18etch1_arm.deb
      Size/MD5 checksum:    53476 47162b2a03a9ad055977aa4d27cd740a
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-footbridge_2.6.18.dfsg.1-18etch1_arm.deb
      Size/MD5 checksum:   233036 132b09dfa75cbb20423f3bb6c275594b
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-iop32x_2.6.18.dfsg.1-18etch1_arm.deb
      Size/MD5 checksum:   233220 d5215cbb53b9c184cebfc5dc72893ccb
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-ixp4xx_2.6.18.dfsg.1-18etch1_arm.deb
      Size/MD5 checksum:   238610 59e6e33844d825f74d82f8dba48cb1e8
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-rpc_2.6.18.dfsg.1-18etch1_arm.deb
      Size/MD5 checksum:   198024 8c36716f4c60229367a105b7e4325212
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-s3c2410_2.6.18.dfsg.1-18etch1_arm.deb
      Size/MD5 checksum:   203280 11631f48347e60f5af0158cdc1f8075d
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-footbridge_2.6.18.dfsg.1-18etch1_arm.deb
      Size/MD5 checksum:  7565524 a2a286b0690c23f4ec56c856fcff80d4
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-iop32x_2.6.18.dfsg.1-18etch1_arm.deb
      Size/MD5 checksum:  7923878 1d9b28341eae31755932c159b5892ef8
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-ixp4xx_2.6.18.dfsg.1-18etch1_arm.deb
      Size/MD5 checksum:  8869840 16111d3e7373c7d15eb288378bcc2a8b
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-rpc_2.6.18.dfsg.1-18etch1_arm.deb
      Size/MD5 checksum:  4585922 14cf94060a4f6c42937b1ebda0280d0a
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-s3c2410_2.6.18.dfsg.1-18etch1_arm.deb
      Size/MD5 checksum:  5008784 72c61e5a9f0c087187a8d55f95c66d04

  HP Precision architecture:

    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6_2.6.18.dfsg.1-18etch1_hppa.deb
      Size/MD5 checksum:  2966804 9878c4fc905f44cb03698d72cb577621
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all_2.6.18.dfsg.1-18etch1_hppa.deb
      Size/MD5 checksum:    53416 267f86fe61fb423690cec3c837f1028a
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all-hppa_2.6.18.dfsg.1-18etch1_hppa.deb
      Size/MD5 checksum:    53442 764739999cea691ce14279402b23b8d7
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-parisc_2.6.18.dfsg.1-18etch1_hppa.deb
      Size/MD5 checksum:   190454 12789846b0987044824d2da63bdcf9f5
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-parisc-smp_2.6.18.dfsg.1-18etch1_hppa.deb
      Size/MD5 checksum:   191232 d8163b5e52fff76ec436bb313a6c033b
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-parisc64_2.6.18.dfsg.1-18etch1_hppa.deb
      Size/MD5 checksum:   191024 9271b455d26056b166b1ca07ca4424ef
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-parisc64-smp_2.6.18.dfsg.1-18etch1_hppa.deb
      Size/MD5 checksum:   191626 59bc531ddc818f74b1477896cdf156cd
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-parisc_2.6.18.dfsg.1-18etch1_hppa.deb
      Size/MD5 checksum: 10500128 ff5b25972c384606f947f177429a7a5c
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-parisc-smp_2.6.18.dfsg.1-18etch1_hppa.deb
      Size/MD5 checksum: 10943960 f05d34a5dd6f432d857d1bc79d996f39
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-parisc64_2.6.18.dfsg.1-18etch1_hppa.deb
      Size/MD5 checksum: 11352084 ad8ae1755cbf880f19c7ab679b037aea
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-parisc64-smp_2.6.18.dfsg.1-18etch1_hppa.deb
      Size/MD5 checksum: 11757822 a32096a8070842fda4ea19a64e1e9fed

  Intel IA-32 architecture:

    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6_2.6.18.dfsg.1-18etch1_i386.deb
      Size/MD5 checksum:  3225042 4218e0f0e5c5fe639d3f2c19e85ae688
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-486_2.6.18.dfsg.1-18etch1_i386.deb
      Size/MD5 checksum:   285462 eea7c9d02758a2a28b326041a858a3d0
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-686_2.6.18.dfsg.1-18etch1_i386.deb
      Size/MD5 checksum:   282480 87f961c91c166aea7eb1cf1f5c79c6a3
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-686-bigmem_2.6.18.dfsg.1-18etch1_i386.deb
      Size/MD5 checksum:   282854 df89a9f4d1e7ce9f8ac8ab9604f4a8aa
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all_2.6.18.dfsg.1-18etch1_i386.deb
      Size/MD5 checksum:    53420 4abb1004d25c2895562ec803f5b6b9ac
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all-i386_2.6.18.dfsg.1-18etch1_i386.deb
      Size/MD5 checksum:    53478 3bde467e3fd60b4cdd8f3ab6b4820e53
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-amd64_2.6.18.dfsg.1-18etch1_i386.deb
      Size/MD5 checksum:   274154 10dda65c9444d16293f7ce3010059d13
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-k7_2.6.18.dfsg.1-18etch1_i386.deb
      Size/MD5 checksum:   283276 24c807eccc8b14e48ce58f6c1cfaf8b0
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-vserver_2.6.18.dfsg.1-18etch1_i386.deb
      Size/MD5 checksum:  3111440 e3a2ed1994c5388e4adcf5da39107104
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-vserver-686_2.6.18.dfsg.1-18etch1_i386.deb
      Size/MD5 checksum:   281034 2601cd40d0e4de13b40e295fbba6857a
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-vserver-k7_2.6.18.dfsg.1-18etch1_i386.deb
      Size/MD5 checksum:   281192 028f100550c07558c8657ee12236e555
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-xen_2.6.18.dfsg.1-18etch1_i386.deb
      Size/MD5 checksum:  3206820 da840b21564212553039e487a41c3e4a
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-xen-686_2.6.18.dfsg.1-18etch1_i386.deb
      Size/MD5 checksum:   274194 d208d7319b225265a9cd8a7cde545a1c
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-xen-vserver_2.6.18.dfsg.1-18etch1_i386.deb
      Size/MD5 checksum:  3230848 ce365b29e4885c42d2ac40f380fb49b4
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-xen-vserver-686_2.6.18.dfsg.1-18etch1_i386.deb
      Size/MD5 checksum:   274398 7ef59e558b2fc9da773a6fd66e8df087
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-486_2.6.18.dfsg.1-18etch1_i386.deb
      Size/MD5 checksum: 16307210 1fbf35f84845b3b8cc9336827794a028
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-686_2.6.18.dfsg.1-18etch1_i386.deb
      Size/MD5 checksum: 16452102 df37f1243a791a6fa2cd2317bfd7ae1c
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-686-bigmem_2.6.18.dfsg.1-18etch1_i386.deb
      Size/MD5 checksum: 16528368 60e83ef38f8a96a61d4ca271a393a770
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-amd64_2.6.18.dfsg.1-18etch1_i386.deb
      Size/MD5 checksum: 16916628 dd59564026f36522ae510be3937fb07c
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-k7_2.6.18.dfsg.1-18etch1_i386.deb
      Size/MD5 checksum: 16585538 e7784fc3ebb5a4ddc7b0cb4ff0728736
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-vserver-686_2.6.18.dfsg.1-18etch1_i386.deb
      Size/MD5 checksum: 16498152 b2475f60454170db949b306a77805918
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-vserver-k7_2.6.18.dfsg.1-18etch1_i386.deb
      Size/MD5 checksum: 16628086 65db69728829d1c1921e65af1aa2b449
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-xen-686_2.6.18.dfsg.1-18etch1_i386.deb
      Size/MD5 checksum:  1298914 46e398fa8d60075c456e6f95b60b46bd
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-xen-vserver-686_2.6.18.dfsg.1-18etch1_i386.deb
      Size/MD5 checksum:  1326272 a047520d88897840b5697fb8c5232a99
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-modules-2.6.18-6-xen-686_2.6.18.dfsg.1-18etch1_i386.deb
      Size/MD5 checksum: 14370222 f8fffe8f30b2422723063fb8121656ab
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-modules-2.6.18-6-xen-vserver-686_2.6.18.dfsg.1-18etch1_i386.deb
      Size/MD5 checksum: 14382584 30bca3849a67bd23ded608771fa4f7b4
    http://security.debian.org/pool/updates/main/l/linux-2.6/xen-linux-system-2.6.18-6-xen-686_2.6.18.dfsg.1-18etch1_i386.deb
      Size/MD5 checksum:    53406 89ec585789ffdd7c48daffbf7a3dd439
    http://security.debian.org/pool/updates/main/l/linux-2.6/xen-linux-system-2.6.18-6-xen-vserver-686_2.6.18.dfsg.1-18etch1_i386.deb
      Size/MD5 checksum:    53422 a7438e4cae438b3f7ecd073a33c41c0b
    http://security.debian.org/pool/updates/main/f/fai-kernels/fai-kernels_1.17+etch.18etch1_i386.deb
      Size/MD5 checksum:  5503064 f19755f1460aadb94f355e4b601e90e5
    http://security.debian.org/pool/updates/main/u/user-mode-linux/user-mode-linux_2.6.18-1um-2etch.18etch1_i386.deb
      Size/MD5 checksum: 25585940 1d2290c410d6d56c0e698f217ddb1dc6

  Intel IA-64 architecture:

    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6_2.6.18.dfsg.1-18etch1_ia64.deb
      Size/MD5 checksum:  3081188 d437965a91ae9ac56952997e2d33ad0b
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all_2.6.18.dfsg.1-18etch1_ia64.deb
      Size/MD5 checksum:    53420 2033b801dd14b62f8be443121002d527
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all-ia64_2.6.18.dfsg.1-18etch1_ia64.deb
      Size/MD5 checksum:    53440 e6b5b2bf1103a890c0ea905122dd024e
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-itanium_2.6.18.dfsg.1-18etch1_ia64.deb
      Size/MD5 checksum:   254800 c0b112b2a6e1530acc82951eb49518f9
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-mckinley_2.6.18.dfsg.1-18etch1_ia64.deb
      Size/MD5 checksum:   254700 8d8700caa62359fc3e47f1053e035e77
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-itanium_2.6.18.dfsg.1-18etch1_ia64.deb
      Size/MD5 checksum: 28013704 f83e98bc516b724786d9e85450a0ec2f
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-mckinley_2.6.18.dfsg.1-18etch1_ia64.deb
      Size/MD5 checksum: 28180622 892500c8c8c370071071dcdf21e0a58c

  Big endian MIPS architecture:

    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6_2.6.18.dfsg.1-18etch1_mips.deb
      Size/MD5 checksum:  3412428 d0abc1f68c5dcf86d12dd89b636744e3
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all_2.6.18.dfsg.1-18etch1_mips.deb
      Size/MD5 checksum:    53428 ed744a8af5a2d4aa5f4d1efdcb34ac88
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all-mips_2.6.18.dfsg.1-18etch1_mips.deb
      Size/MD5 checksum:    53480 b6231a763d53c20905d4ad4be7c9199e
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-qemu_2.6.18.dfsg.1-18etch1_mips.deb
      Size/MD5 checksum:   153598 241098dc798f9daadf5cb7540e0c722d
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-r4k-ip22_2.6.18.dfsg.1-18etch1_mips.deb
      Size/MD5 checksum:   162206 2995b5d456a77a73dddac23bb93579cf
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-r5k-ip32_2.6.18.dfsg.1-18etch1_mips.deb
      Size/MD5 checksum:   166464 4cfee28543a7d5d87dc97e81b3ee4e7f
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-sb1-bcm91250a_2.6.18.dfsg.1-18etch1_mips.deb
      Size/MD5 checksum:   186236 e0ea496e319ba5a6bb96158dfb095dfb
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-sb1a-bcm91480b_2.6.18.dfsg.1-18etch1_mips.deb
      Size/MD5 checksum:   185976 fef8110dd77f90dd9d20897a6ffce64a
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-qemu_2.6.18.dfsg.1-18etch1_mips.deb
      Size/MD5 checksum:  6125364 21b89e3ceb419401eba149c2071499af
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-r4k-ip22_2.6.18.dfsg.1-18etch1_mips.deb
      Size/MD5 checksum:  8307650 67e035e6444f296510d4be8455a69321
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-r5k-ip32_2.6.18.dfsg.1-18etch1_mips.deb
      Size/MD5 checksum:  9074780 efaa558418909d4dadf02726edf717e4
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-sb1-bcm91250a_2.6.18.dfsg.1-18etch1_mips.deb
      Size/MD5 checksum: 15678520 ba084d91886dbb913a64dc39f9f0604e
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-sb1a-bcm91480b_2.6.18.dfsg.1-18etch1_mips.deb
      Size/MD5 checksum: 15653180 83d666463b781dd56fb9b4ee75dc5aa3

  Little endian MIPS architecture:

    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6_2.6.18.dfsg.1-18etch1_mipsel.deb
      Size/MD5 checksum:  3349366 54ede4ebe4c475f818adbb9f9b557a82
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all_2.6.18.dfsg.1-18etch1_mipsel.deb
      Size/MD5 checksum:    53420 d846a319d6d7cdbf13c35d105915df6e
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all-mipsel_2.6.18.dfsg.1-18etch1_mipsel.deb
      Size/MD5 checksum:    53480 7e7bec6082a7ce3890521658d551d55c
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-qemu_2.6.18.dfsg.1-18etch1_mipsel.deb
      Size/MD5 checksum:   149410 bc8196003bc11f0a3d0b369ca49d6147
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-r3k-kn02_2.6.18.dfsg.1-18etch1_mipsel.deb
      Size/MD5 checksum:   155242 b448b722ed93eda5dc943d18eca1347d
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-r4k-kn04_2.6.18.dfsg.1-18etch1_mipsel.deb
      Size/MD5 checksum:   155274 8457b39ace9a0f22ebae206b16c1b4fe
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-r5k-cobalt_2.6.18.dfsg.1-18etch1_mipsel.deb
      Size/MD5 checksum:   177718 5a9aea936081ad389308b073d9f699c9
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-sb1-bcm91250a_2.6.18.dfsg.1-18etch1_mipsel.deb
      Size/MD5 checksum:   182328 5d4916cba7a5545f34725dbab6b5d66b
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-sb1a-bcm91480b_2.6.18.dfsg.1-18etch1_mipsel.deb
      Size/MD5 checksum:   182032 fdbce00701e0fa314ea7581687ed2508
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-qemu_2.6.18.dfsg.1-18etch1_mipsel.deb
      Size/MD5 checksum:  6027142 4552b40ffab584bf1a6b82c786f7a3f1
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-r3k-kn02_2.6.18.dfsg.1-18etch1_mipsel.deb
      Size/MD5 checksum:  5940884 123504ffec96ffb497c0aa86bd96145d
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-r4k-kn04_2.6.18.dfsg.1-18etch1_mipsel.deb
      Size/MD5 checksum:  5925822 904ca2bf1f31df74032782aee24bfbe9
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-r5k-cobalt_2.6.18.dfsg.1-18etch1_mipsel.deb
      Size/MD5 checksum:  9862730 fb44861726c3dd6c49981962b0e75345
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-sb1-bcm91250a_2.6.18.dfsg.1-18etch1_mipsel.deb
      Size/MD5 checksum: 15052630 96c064be52760122fd78cedb106cdf5e
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-sb1a-bcm91480b_2.6.18.dfsg.1-18etch1_mipsel.deb
      Size/MD5 checksum: 15028652 c265e988c734f5622e462ffaa955182d

  PowerPC architecture:

    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6_2.6.18.dfsg.1-18etch1_powerpc.deb
      Size/MD5 checksum:  3391566 f6cf26bd757d1970043d1ea811eb6ea2
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all_2.6.18.dfsg.1-18etch1_powerpc.deb
      Size/MD5 checksum:    53424 d6b1b6643b60c2cf52de77fbfaa73322
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all-powerpc_2.6.18.dfsg.1-18etch1_powerpc.deb
      Size/MD5 checksum:    53472 7d38034c2ed8b6e413460726456b0511
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-powerpc_2.6.18.dfsg.1-18etch1_powerpc.deb
      Size/MD5 checksum:   250524 784f7accc7ecc32e4611e98d9c187764
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-powerpc-miboot_2.6.18.dfsg.1-18etch1_powerpc.deb
      Size/MD5 checksum:   228658 2ae8d9ad2112a8d72198fcf2e9cc51c0
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-powerpc-smp_2.6.18.dfsg.1-18etch1_powerpc.deb
      Size/MD5 checksum:   251470 06c84ff8ae37d7dbc3a97f77c14b22d1
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-powerpc64_2.6.18.dfsg.1-18etch1_powerpc.deb
      Size/MD5 checksum:   251880 ff0b77479f8c25db4fc98eec75d00949
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-prep_2.6.18.dfsg.1-18etch1_powerpc.deb
      Size/MD5 checksum:   245396 58eb019d0d2d1220103dda9dc402f46a
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-vserver_2.6.18.dfsg.1-18etch1_powerpc.deb
      Size/MD5 checksum:  3413854 236195a1d6538e9f645ba5ef1844da28
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-vserver-powerpc_2.6.18.dfsg.1-18etch1_powerpc.deb
      Size/MD5 checksum:   252636 0704bb1ab645a919ef95ec8c44cd69df
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-vserver-powerpc64_2.6.18.dfsg.1-18etch1_powerpc.deb
      Size/MD5 checksum:   252980 c3ce872d9cc422d67173864e9aed8d22
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-powerpc_2.6.18.dfsg.1-18etch1_powerpc.deb
      Size/MD5 checksum: 16625424 28d1539a85a973cda7805c21d443f2fb
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-powerpc-miboot_2.6.18.dfsg.1-18etch1_powerpc.deb
      Size/MD5 checksum: 15156612 e938711b4fedaf5697062624bca57979
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-powerpc-smp_2.6.18.dfsg.1-18etch1_powerpc.deb
      Size/MD5 checksum: 16966834 7e8d5e72abac6e1a01f4f33ca9067e5a
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-powerpc64_2.6.18.dfsg.1-18etch1_powerpc.deb
      Size/MD5 checksum: 18292932 80f17d2e9f76624a6314a91406222e82
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-prep_2.6.18.dfsg.1-18etch1_powerpc.deb
      Size/MD5 checksum: 16401030 2a725fce0170558aa49d3c1774616877
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-vserver-powerpc_2.6.18.dfsg.1-18etch1_powerpc.deb
      Size/MD5 checksum: 17016116 9a74fc1f8f195991606ca3368b778ce4
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-vserver-powerpc64_2.6.18.dfsg.1-18etch1_powerpc.deb
      Size/MD5 checksum: 18347202 a5e5c517ce29c949d8fcf7da33a6cf54
    http://security.debian.org/pool/updates/main/f/fai-kernels/fai-kernels_1.17+etch.18etch1_powerpc.deb
      Size/MD5 checksum:  3366334 479ac86885e04760b92b512e171e7eb0

  IBM S/390 architecture:

    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6_2.6.18.dfsg.1-18etch1_s390.deb
      Size/MD5 checksum:  2942294 5001340969644792fc29d3747a4797f8
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all_2.6.18.dfsg.1-18etch1_s390.deb
      Size/MD5 checksum:    53418 aabcb9e7514f03258c0316b3b2ef45f3
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all-s390_2.6.18.dfsg.1-18etch1_s390.deb
      Size/MD5 checksum:    53436 46ce3c7f73df4137cdb772470b68a5a7
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-s390_2.6.18.dfsg.1-18etch1_s390.deb
      Size/MD5 checksum:   142562 00777b961f7edd8e6188bff95eddb485
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-s390x_2.6.18.dfsg.1-18etch1_s390.deb
      Size/MD5 checksum:   142998 679ee41aff912fa8ef11a6ff635b3081
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-vserver_2.6.18.dfsg.1-18etch1_s390.deb
      Size/MD5 checksum:  2965340 8dc36dfb19566dd1d33f6772f3d989ce
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-vserver-s390x_2.6.18.dfsg.1-18etch1_s390.deb
      Size/MD5 checksum:   143856 9c1af4e31e728c477b1a2ff2bd2e5584
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-s390_2.6.18.dfsg.1-18etch1_s390.deb
      Size/MD5 checksum:  5401206 d90ae8bb83a230e16c5caf4ec4587429
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-s390-tape_2.6.18.dfsg.1-18etch1_s390.deb
      Size/MD5 checksum:  1437980 030e11553c2b6713e63b55b1431b2ac5
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-s390x_2.6.18.dfsg.1-18etch1_s390.deb
      Size/MD5 checksum:  5619132 83aac920b1b9de8e5811ce9af5005744
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-vserver-s390x_2.6.18.dfsg.1-18etch1_s390.deb
      Size/MD5 checksum:  5661404 97c3bae25cf2e1f52620293ea821e884

  Sun Sparc architecture:

    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6_2.6.18.dfsg.1-18etch1_sparc.deb
      Size/MD5 checksum:  3167368 79420add5efa5e0a629e153441d0973d
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all_2.6.18.dfsg.1-18etch1_sparc.deb
      Size/MD5 checksum:    53412 7498e949bbf468adf1a738d9428cc5ec
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-all-sparc_2.6.18.dfsg.1-18etch1_sparc.deb
      Size/MD5 checksum:    53444 4120e1f2e95b06b45d8d99ae0c90ffd4
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-sparc32_2.6.18.dfsg.1-18etch1_sparc.deb
      Size/MD5 checksum:   164418 7da5dd792865b08f2422e67d3f24c2d9
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-sparc64_2.6.18.dfsg.1-18etch1_sparc.deb
      Size/MD5 checksum:   193702 8bfb928e318e553063e358aa6e143895
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-sparc64-smp_2.6.18.dfsg.1-18etch1_sparc.deb
      Size/MD5 checksum:   194110 e2a814379afab4316b779b58b1d56c7d
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-vserver_2.6.18.dfsg.1-18etch1_sparc.deb
      Size/MD5 checksum:  3189710 fa05c7cc8aafef2b9c0d9a10eca9314e
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-headers-2.6.18-6-vserver-sparc64_2.6.18.dfsg.1-18etch1_sparc.deb
      Size/MD5 checksum:   194634 c296eeb3746d0c544e993ab3b0c0d2e8
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-sparc32_2.6.18.dfsg.1-18etch1_sparc.deb
      Size/MD5 checksum:  6410380 b78a7576b56b3bfebbece6bbbd9681da
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-sparc64_2.6.18.dfsg.1-18etch1_sparc.deb
      Size/MD5 checksum: 10389804 98197dd18310d2c3df4d3f35709fd133
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-sparc64-smp_2.6.18.dfsg.1-18etch1_sparc.deb
      Size/MD5 checksum: 10647366 201188cd730d7e8bd1867f7a138d0ef6
    http://security.debian.org/pool/updates/main/l/linux-2.6/linux-image-2.6.18-6-vserver-sparc64_2.6.18.dfsg.1-18etch1_sparc.deb
      Size/MD5 checksum: 10690824 b4e60ee25ff43212b942ab92ba19c43c

  These files will probably be moved into the stable distribution on
  its next update.

---------------------------------------------------------------------------------
For apt-get: deb http://security.debian.org/ stable/updates main
For dpkg-ftp: ftp://security.debian.org/debian-security dists/stable/updates/main
Mailing list: debian-security-announce@lists.debian.org
Package info: `apt-cache show <pkg>' and http://packages.debian.org/<pkg>