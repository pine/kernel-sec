----------------------------------------------------------------------
Debian Security Advisory DSA-2443-1                security@debian.org
http://www.debian.org/security/                           Dann Frazier
March 26, 2012                      http://www.debian.org/security/faq
----------------------------------------------------------------------

Package        : linux-2.6
Vulnerability  : privilege escalation/denial of service
Problem type   : local
Debian-specific: no
CVE Id(s)      : CVE-2009-4307 CVE-2011-1833 CVE-2011-4347 CVE-2012-0045
                 CVE-2012-1090 CVE-2012-1097

Several vulnerabilities have been discovered in the Linux kernel that may lead
to a denial of service or privilege escalation. The Common Vulnerabilities and
Exposures project identifies the following problems:

CVE-2009-4307

    Nageswara R Sastry reported an issue in the ext4 filesystem. Local users
    with the privileges to mount a filesystem can cause a denial of service
    (BUG) by providing a s_log_groups_per_flex value greater than 31.

CVE-2011-1833

    Vasiliy Kulikov of Openwall and Dan Rosenberg discovered an information
    leak in the eCryptfs filesystem. Local users were able to mount arbitrary
    directories.

CVE-2011-4347

    Sasha Levin reported an issue in the device assignment functionality in
    KVM. Local users with permission to access /dev/kvm could assign unused pci
    devices to a guest and cause a denial of service (crash).

CVE-2012-0045

    Stephan Barwolf reported an issue in KVM. Local users in a 32-bit guest
    running on a 64-bit system can crash the guest with a syscall instruction.

CVE-2012-1090

    CAI Qian reported an issue in the CIFS filesystem. A reference count leak
    can occur during the lookup of special files, resulting in a denial of
    service (oops) on umount.

CVE-2012-1097

    H. Peter Anvin reported an issue in the regset infrastructure. Local users
    can cause a denial of service (NULL pointer dereference) by triggering the
    write methods of readonly regsets.

For the stable distribution (squeeze), this problem has been fixed in version
2.6.32-41squeeze2.

The following matrix lists additional source packages that were rebuilt for
compatibility with or to take advantage of this update:

                                             Debian 6.0 (squeeze)
     user-mode-linux                         2.6.32-1um-4+41squeeze2

We recommend that you upgrade your linux-2.6 and user-mode-linux packages.

Thanks to Micah Anderson for proof reading this text.

Further information about Debian Security Advisories, how to apply
these updates to your system and frequently asked questions can be
found at: http://www.debian.org/security/

Mailing list: debian-security-announce@lists.debian.org
