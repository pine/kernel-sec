Package        : linux
CVE ID         : CVE-2016-6786 CVE-2016-6787 CVE-2016-8405 CVE-2016-9191
                 CVE-2017-2583 CVE-2017-2584 CVE-2017-2596 CVE-2017-2618
                 CVE-2017-5549 CVE-2017-5551 CVE-2017-5897 CVE-2017-5970
                 CVE-2017-6001 CVE-2017-6074

Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or have other
impacts.

CVE-2016-6786 / CVE-2016-6787

    It was discovered that the performance events subsystem does not
    properly manage locks during certain migrations, allowing a local
    attacker to escalate privileges.  This can be mitigated by
    disabling unprivileged use of performance events:
    sysctl kernel.perf_event_paranoid=3

CVE-2016-8405

    Peter Pi of Trend Micro discovered that the frame buffer video
    subsystem does not properly check bounds while copying color maps to
    userspace, causing a heap buffer out-of-bounds read, leading to
    information disclosure.

CVE-2016-9191

    CAI Qian discovered that reference counting is not properly handled
    within proc_sys_readdir in the sysctl implementation, allowing a
    local denial of service (system hang) or possibly privilege
    escalation.

CVE-2017-2583

    Xiaohan Zhang reported that KVM for amd64 does not correctly
    emulate loading of a null stack selector.  This can be used by a
    user in a guest VM for denial of service (on an Intel CPU) or to
    escalate privileges within the VM (on an AMD CPU).

CVE-2017-2584

    Dmitry Vyukov reported that KVM for x86 does not correctly emulate
    memory access by the SGDT and SIDT instructions, which can result
    in a use-after-free and information leak.

CVE-2017-2596

    Dmitry Vyukov reported that KVM leaks page references when
    emulating a VMON for a nested hypervisor.  This can be used by a
    privileged user in a guest VM for denial of service or possibly
    to gain privileges in the host.

CVE-2017-2618

    It was discovered that an off-by-one in the handling of SELinux
    attributes in /proc/pid/attr could result in local denial of
    service.

CVE-2017-5549

    It was discovered that the KLSI KL5KUSB105 serial USB device
    driver could log the contents of uninitialised kernel memory,
    resulting in an information leak.

CVE-2017-5551

    Jan Kara found that changing the POSIX ACL of a file on tmpfs never
    cleared its set-group-ID flag, which should be done if the user
    changing it is not a member of the group-owner. In some cases, this
    would allow the user-owner of an executable to gain the privileges
    of the group-owner.

CVE-2017-5897

    Andrey Konovalov discovered an out-of-bounds read flaw in the
    ip6gre_err function in the IPv6 networking code.

CVE-2017-5970

    Andrey Konovalov discovered a denial-of-service flaw in the IPv4
    networking code.  This can be triggered by a local or remote
    attacker if a local UDP or raw socket has the IP_RETOPTS option
    enabled.

CVE-2017-6001

    Di Shen discovered a race condition between concurrent calls to
    the performance events subsystem, allowing a local attacker to
    escalate privileges. This flaw exists because of an incomplete fix
    of CVE-2016-6786.  This can be mitigated by disabling unprivileged
    use of performance events: sysctl kernel.perf_event_paranoid=3

CVE-2017-6074

    Andrey Konovalov discovered a use-after-free vulnerability in the
    DCCP networking code, which could result in denial of service or
    local privilege escalation.  On systems that do not already have
    the dccp module loaded, this can be mitigated by disabling it:
    echo >> /etc/modprobe.d/disable-dccp.conf install dccp false

For the stable distribution (jessie), these problems have been fixed in
version 3.16.39-1+deb8u1.
